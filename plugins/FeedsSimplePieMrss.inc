<?php
// $Id$

/**
 * Class definition for SimplePie extension MRSS parser.
 *
 * Parses MRSS and Atom with MRSS feeds. Will probably do iTunes too.
 */
class FeedsSimplePieMrss extends FeedsSimplePieParser {

  /*
   * Allow extension of FeedsSimplePie item parsing.
   */
  protected function parseExtensions(&$item, $simplepie_item) {
    $enclosures = $simplepie_item->get_enclosures();
    if (is_array($enclosures)) {
      foreach ($enclosures as $enclosure) {
        // thumbnails - from mrss just one set per item
        $item['enclosures_thumbnail'] = $enclosure->get_thumbnails();
        // keep the largest enclosure
        // could be a mapping option really, but we're here anyway..
        if ($this->isVideo($enclosure->get_real_type())) {
          if (! isset($enclosures_largest) || $enclosures_largest->get_size() < $enclosure->get_size()) {
            $enclosures_largest = $enclosure;
          }
        }
      }
    }
    if (isset($enclosures_largest)) {
      $item['enclosures_largest'] = array(new FeedsSimplePieEnclosure($enclosures_largest));
    }
  }

  /**
   * Return mapping sources array.
   */
  public function getMappingSources() {
    $sources = parent::getMappingSources();
    $sources['enclosures_thumbnail'] = array(
      'name' => t('Enclosures: thumbnails'),
      'description' => t('An array of thumbnails attached to enclosures.'),
    );
    $sources['enclosures_largest'] = array(
      'name' => t('Enclosures: largest video'),
      'description' => t('The largest enclosed video.'),
    );
    return $sources;
  }

  /**
   * Test if string mime-type is a video type, return bool.
   */
  private function isVideo($mime) {
    $accept_types = array('video/avi', 'video/msvideo', 'video/x-msvideo', 'video/x-dv', 'video/x-flv', 'video/mpeg', 'video/x-mpeg', 'video/x-peq2a', 'video/mp4', 'video/quicktime', 'application/x-troff-msvideo', 'application/x-shockwave-flash', 'flv-application/octet-stream');
    return in_array($mime, $accept_types);
  }
}
