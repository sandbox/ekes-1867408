<?php
// $Id$

/**
 * Class definition for Node Processor with alternative hashing.
 *
 * Simplepie MRSS item sometimes causes problems with the hash changing,
 * due to the order of elements rather than their content. This just makes
 * a simple change to the hashing.
 */
class FeedsNodeProcessorMrss extends FeedsNodeProcessor {
  public function hash($item) {
    if (isset($item['raw'])) {
      unset($item['raw']);
    }

    $serialized_mappings = var_export($this->config['mappings'], true);
    if ($debug) {
      return array('string' => var_export($item, true) . $serialized_mappings,
                   'hash' => hash('md5', var_export($item, true) . $serialized_mappings));
    }
    return hash('md5', var_export($item, true) . $serialized_mappings);
  }
}
